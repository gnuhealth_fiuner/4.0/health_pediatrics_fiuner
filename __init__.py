from trytond.pool import Pool
from . import health_pediatrics_fiuner

def register():
    Pool.register(
        health_pediatrics_fiuner.NewBorn,
        module='health_pediatrics_fiuner', type_='model')
